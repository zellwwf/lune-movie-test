module MoviesHelper

  def movie_rating(movie)
    return 0 unless movie.ratings_count > 0

    return  movie.ratings_sum / movie.ratings_count
  end
end
