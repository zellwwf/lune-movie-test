class ReviewImporter < CsvImporter
  EXPECTED_HEADERS = Set.new(["Movie", "User", "Stars", "Review"]).freeze

  private

  def import_data(dataset)
    dataset.each do |row|
      # Find the movie by its name
      movie = Movie.find_by(name: row['Movie'])

      if movie
        # Map CSV columns to Review attributes
        review_attributes = {
          username: row['User'],
          rating: row['Stars'],
          review: row['Review'],
          movie: movie
        }

        # Create the review
        Review.create!(review_attributes)
      else
        Rails.logger.warn "Movie not found: #{row['Movie']}"
      end
    end
  end

  def expected_headers
    EXPECTED_HEADERS
  end

  def generate_key(row_hash)
    [row_hash['Movie'], row_hash['User']]
  end

  def append_values
    []
  end
end
