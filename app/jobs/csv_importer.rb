require 'csv'
require 'set'

class CsvImporter < ActiveJob::Base
  queue_as :default

  def perform(file_path)
    raw_data, headers = read_csv(file_path)
    dataset = preprocess(raw_data, headers)
    dataset = clean(dataset)

    import_data(dataset)
  end

  private

  def read_csv(file_path)
    csv_data = CSV.readlines(file_path)
    headers = csv_data.shift
    headers_set = headers.to_set

    raise ArgumentError, "Bad CSV Structure" unless headers_set == expected_headers

    [csv_data, headers]
  end

  def preprocess(dataset, headers)
    thingy = {}

    append_value = Proc.new do |key, value, row_hash|
      (Array(thingy[key][value]) + [row_hash[value]]).compact.uniq.map(&:strip)
    end

    dataset.each do |row|
      row_hash = Hash[headers.zip(row)]
      key = generate_key(row_hash)
      thingy[key] ||= row_hash
      append_values.each do |value|
        thingy[key][value] = append_value.call(key, value, row_hash)
      end
    end

    thingy.values
  end

  def clean(dataset)
    # Remove rows with nil values
    dataset.reject! { |row| row.values.any?(&:nil?) }

    dataset
  end

  def import_data(dataset)
    raise NotImplementedError, "Subclasses must implement this method"
  end

  def expected_headers
    raise NotImplementedError, "Subclasses must implement this method"
  end

  def generate_key(row_hash)
    raise NotImplementedError, "Subclasses must implement this method"
  end

  def append_values
    raise NotImplementedError, "Subclasses must implement this method"
  end

  def append_value
    Proc.new do |key, value, row_hash|
      (Array(thingy[key][value]) + [row_hash[value]]).compact.uniq.map(&:strip)
    end
  end
end
