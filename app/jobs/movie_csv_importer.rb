class MovieCsvImporter < CsvImporter
  EXPECTED_HEADERS = Set.new(["Movie", "Description", "Year", "Director", "Actor", "Filming location", "Country"]).freeze

  private

  def import_data(dataset)
    dataset.each do |row|
      movie_attributes = {
        name: row['Movie'],
        description: row['Description'],
        year: row['Year'],
        director: row['Director'],
        actors: row['Actor'],
        cities: row['Filming location'],
        countries: row['Country']
      }

      Movie.find_or_create_by!(movie_attributes)
    end
  end

  def expected_headers
    EXPECTED_HEADERS
  end

  def generate_key(row_hash)
    [row_hash['Movie'], row_hash['Director'], row_hash['Year']]
  end

  def append_values
    ['Actor', 'Filming location', 'Country']
  end
end
