class Review < ApplicationRecord
  belongs_to :movie

  after_create :update_movie_ratings
  after_destroy :update_movie_ratings

  private

  def update_movie_ratings
    movie.update_ratings
  end
end
