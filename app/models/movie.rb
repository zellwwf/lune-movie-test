class Movie < ApplicationRecord
  has_many :reviews

  def update_ratings
    self.ratings_sum = reviews.sum(:rating)
    self.ratings_count = reviews.count
    save
  end
end
