class MoviesController < ApplicationController

  def index
    if params[:actor].present?
      @movies = Movie.where("EXISTS (SELECT 1 FROM unnest(actors) actor WHERE actor ILIKE ?)", "%#{params[:actor]}%")
    else
      @movies = Movie.all
    end

    # Calculate average rating and sort movies
    @movies = @movies.select('movies.*, COALESCE(ratings_sum / NULLIF(ratings_count, 0), 0) AS average_rating')
                     .order('average_rating DESC')
  end

  def show
    @movie = Movie.find(params[:id])
  end

  def import_csv
    csv_file = params[:csv_file]

    path = Rails.root.join('tmp', csv_file.original_filename)
    File.open(path, 'wb') do |file|
      file.write(csv_file.read)
    end

    MovieCsvImporter.perform_later(path.to_s)

    redirect_to movies_path, notice: 'CSV import started.'
  end

  def import_reviews
    csv_file = params[:reviews_file]

    path = Rails.root.join('tmp', csv_file.original_filename)
    File.open(path, 'wb') do |file|
      file.write(csv_file.read)
    end

    ReviewImporter.perform_later(path.to_s)

    redirect_to movies_path, notice: 'Reviews import started.'
  end
end
