class CreateMovies < ActiveRecord::Migration[7.1]
  def change
    create_table :movies do |t|
      t.string :name
      t.string :description
      t.string :director
      t.string :actors, array: true, default: []
      t.integer :year
      t.string :cities, array: true, default: []
      t.string :countries, array: true, default: []

      t.float :ratings_sum, default: 0
      t.float :ratings_count, default: 0

      t.timestamps
    end

    add_index :movies, :actors, using: 'gin'
    add_index :movies, [:name, :director, :year]
  end
end
