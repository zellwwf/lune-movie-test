class CreateReviews < ActiveRecord::Migration[7.1]
  def change
    create_table :reviews do |t|
      t.string :username
      t.string :review
      t.references :movie, null: false, foreign_key: true
      t.integer :rating

      t.timestamps
    end

    add_index :reviews, :username
    add_index :reviews, :rating
  end
end
