# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2024_02_29_143255) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "movies", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "director"
    t.string "actors", default: [], array: true
    t.integer "year"
    t.string "cities", default: [], array: true
    t.string "countries", default: [], array: true
    t.float "ratings_sum", default: 0.0
    t.float "ratings_count", default: 0.0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["actors"], name: "index_movies_on_actors", using: :gin
    t.index ["name", "director", "year"], name: "index_movies_on_name_and_director_and_year"
  end

  create_table "reviews", force: :cascade do |t|
    t.string "username"
    t.string "review"
    t.bigint "movie_id", null: false
    t.integer "rating"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["movie_id"], name: "index_reviews_on_movie_id"
    t.index ["rating"], name: "index_reviews_on_rating"
    t.index ["username"], name: "index_reviews_on_username"
  end

  add_foreign_key "reviews", "movies"
end
